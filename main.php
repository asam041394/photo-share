<!DOCTYPE HTML>
<?php
session_start();
include('db.php');
if(empty($_SESSION['login_user']))
{
header('Location: index.php');
}
$ur=$_SESSION['login_user'];

?>
<html>
<head>
    <meta charset="utf-8">
    <title>Photo share</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Html5TemplatesDreamweaver.com">

    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->
    <link href="scripts/icons/general/stylesheets/general_foundicons.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="scripts/icons/social/stylesheets/social_foundicons.css" media="screen" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <link href="http://fonts.googleapis.com/css?family=Syncopate" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet" type="text/css">

    <link href="styles/custom.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery.form.js"></script>

<script type="text/javascript" >
 $(document).ready(function() { 
		
            $('#photoimg').live('change', function()			{ 
			           $("#preview").html('');
			    $("#preview").html('<img src="loader.gif" alt="Uploading...."/>');
			$("#imageform").ajaxForm({
						target: '#preview'
		}).submit();
		
			});
        }); 
</script>
    
    
</head>
<body id="pageBody">

<div id="divBoxed" class="container">

    <div class="transparent-bg" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: -1;zoom: 1;"></div>

    <div class="divPanel notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                     <a href="index.html" id="divSiteTitle">Photo Share</a><br />  

                    </div>
                    <div class="row">

  <form class="form-inline">
  
  <div class="form-group">
    <div id="se">
    <input type="text" class="form-control-lg" id="exampleInputPassword3" placeholder="Search Your Buddy">
</div>
  </div>
  
</form>



</div>
<div id="te">
                    <div id="divMenuRight" class="pull-right">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                        NAVIGATION <span class="icon-chevron-down icon-white"></span>
                        </button>
                            <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                            <li><a href="">Search</a></li>
                            <li><a href="search.php">Find Friends</a></li>
						    <li><a href="main.php">Home</a></li>
						    <li><a href="profile.php">Profile</a></li>						    
                            <li><a href="logout.php">Logout</a></li>
                            </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</div>
            <div class="row-fluid">
                <div class="span12">
                    <div id="contentInnerSeparator"></div>
                </div>
            </div>
    </div>

    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <div class="alert alert-primary" role="alert">
                <h3 class="text-error">Upload your photo which you want share:</h3>
                <form id="imageform" method="post" enctype="multipart/form-data" action='testaj.php'>
<input type="hidden" value="<?php echo date("d/m/Y"); ?>" id="dd" name="dd" >


Upload your image <input type="file" name="photoimg" id="photoimg" />

</form>
<div id='preview'>
</div>
  
                </div>
            </div>
            
<?php


$q=mysqli_query($db,"select * from friends where username='$ur'");

while($zz=mysqli_fetch_array($q))
{  $mn=$zz['friend'];
	$s=mysqli_query($db,"select * from shared where user_name='$mn'");



while($row=mysqli_fetch_array($s))
{
     $a=$row['id'];
	 $b=$row['user_name'];
	 $c=$row['date'];
	 $d=$row['profile_image'];
	$r=mysqli_query($db,"insert into same values('$a','$b','$c','$d')");
}
}

$h=mysqli_query($db,"select * from same order by id DESC");

while($hh=mysqli_fetch_array($h))
{  $hhh=$hh['user_name'];

	$k=mysqli_query($db,"select * from users where username='$hhh'");
     $kk=mysqli_fetch_array($k);
     $kkk=$kk['profile_image'];

?>
            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span12" id="divMain">
                    <div class="alert alert-primary" role="alert">
                        <div class="media">
                    <a class="pull-left" href="#">
                        <img src="<?php echo "uploads/".$kkk;?>" height="64px" width="64px" class="img-rounded" alt="" />
                    </a>
                    <div class="media-body">
                        <h3 class="text-error"><?php echo $hhh; ?></h3>
                        
                    </div>
                </div>

                       
                        <div id="ad">
                        <img src="<?php echo "uploads/".$hh['profile_image']; ?>" height="300px" width="300px" alt="..." class="img-rounded">   
                        </div>
                        <?php echo $hh['date']; ?>
                    </div>
                </div>
            </div>
            <!--END--><?php }
			 ?>
           
           

<?php
mysqli_query($db,"TRUNCATE table same");
?>





</body>
</html>